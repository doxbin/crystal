var express = require('express');
var path = require('path');
var fileupload = require("express-fileupload");
var fs = require('fs');
var app = express();
var router = express.Router();
var WhitelistedExtensions = 
[
    "png",
    "txt",
    "gif",
    "mp4",
    "jpg"
]

if (!fs.existsSync('server.json')) 
{
    fs.writeFileSync('server.json', JSON.stringify
    ({
        Uploads: [],
        Keys: 
        [
            {
                Key: "demo",
                UsedBy: "admin",
                Date: "2020-12-02|13:47"
            }
        ],
        Invites: 
        [
            {
                Code: "crystal",
                Expires: "Never"
            }
        ],
        Domains:
        [
            {
                Domain: "fucked-me.wtf",
                Wildcard: true
            },
            {
                Domain: "discord-exploiter.club",
                Wildcard: false
            },
            {
                Domain: "discord-exploiting.club",
                Wildcard: false
            },
            {
                Domain: "discord-exploiters.club",
                Wildcard: false
            },
            {
                Domain: "get-some.info",
                Wildcard: true
            },
            {
                Domain: "kill-the-je.ws",
                Wildcard: true
            },
            {
                Domain: "slavic.party",
                Wildcard: false
            },
            {
                Domain: "doxbin.cc",
                Wildcard: false
            },
            {
                Domain: "gopnik.party",
                Wildcard: false
            },
            {
                Domain: "nuke-the.us",
                Wildcard: false
            },
            {
                Domain: "kill-the.eu",
                Wildcard: false
            }
        ]
    }, null, 2));
}

var ImageHost = JSON.parse(fs.readFileSync('server.json', 'utf8'));

router.get('/api', (req, res) => {
    res.json({code: 200, Uploads: ImageHost.Uploads.length, Invites: ImageHost.Invites.length, Keys: ImageHost.Keys.length});
});

router.post('/api/upload', (req, res) => {
    try {
        if (!req.headers.key) {
            return res.status(400).json({code: 400, message: "No API Key was provided."});
        }

        if (!req.headers.domain) {
            return res.status(400).json({code: 400, message: "No domain was provided."});
        }

        if (!isValidAPIKey(req.headers.key)) {
            return res.status(401).json({code: 401, message: "Invalid API Key."});
        }

        var file = req.files.file;
        if (!hasValidExtension(file.name)) {
            return res.status(400).json({code: 400, message: "Unsupported file extension."});
        }
       
        if (!isValidDomain(req.headers.domain)) {
            return res.status(400).json({code: 400, message: "Invalid domain."});
        }

        var domainInfo = getDomainInfo(req.headers.domain);
        if (!req.headers.domain.includes(req.get('host'))) {
            return res.status(400).json({code: 400, message: "Domain doesn't match uploading server."});
        }
        
        var name = GenerateString(20);
        var extension = file.name.split('.')[1];
        var userinfo = getUserInfo(req.headers.key);
        var date = new Date().toUTCString()
        var url = `${domainInfo.Wildcard ? `http` : `https`}://${req.headers.domain}/${name}.${extension}`;

        if (!file.name.includes(".txt")) {
            fs.mkdirSync(`./${name}`);
            fs.writeFileSync(`./${name}/index.html`, `<html>
            <head>
              <title>Uploaded by ${userinfo.UsedBy}</title>
              <meta property="og:title" content="Crystal Images">
              <meta property="og:description" content="${file.name.includes(".mp4") ? "Video" : "Image"} uploaded by ${userinfo.UsedBy} on ${date}">
              <meta name="twitter:card" content="summary_large_image">
              <meta property="twitter:image" content="${url}">
              <meta name="theme-color" content="#ff0000">
              <style>
              h1 
              {
                  font-size: 24px;
                  text-align: center;
                  font-family: Consolas;
                  color:  #ffffff;
              }
              
              h2 
              {
                  font-size: 20px;
                  text-align: center;
                  font-family: Consolas;
                  color: #ffffff;
              }

              h3 {
                  font-size: 26px;
                  text-align: center;
                  font-family: Consolas;
                  color: #ffffff;
                  text-decoration: none;
              }

              h3:hover {
                  color: grey;
                  cursor: pointer;
              }
              </style>
            </head>
            <body style='background-color: #212121'>
                <center>
                    <h3 onclick="window.location.href = 'https://discord.gg/pqAUmKgAyK'">Crystal Images</h3>
                    ${!file.name.includes(".mp4") ? `<img src="${domainInfo.Wildcard ? `http://${getDomainInfo(req.headers.domain).Domain}/${name}.${extension}` : url}"></img>` : `<video><source src="${domainInfo.Wildcard ? `http://${getDomainInfo(req.headers.domain).Domain}/${name}.${extension}` : url}"></video>`}
                    <h1>${name}.${extension}</h1>
                    <h2>Uploaded By: ${userinfo.UsedBy} on ${date}</h2>
                </center>
            </body>
          </html>`);
        }
        

        fs.writeFileSync(`./${name}.${extension}`, file.data, 'base64');
        ImageHost.Uploads.push({
            Url: url,
            Uploader: userinfo.UsedBy,
            Date: date
        });
        fs.writeFileSync('server.json', JSON.stringify(ImageHost, null, 2));
        res.send(url);
    }
    catch(err) {
        return res.status(500).json({code: 500, message: `Internal Server Error. ${err}`});
    }
});

app.use(express.static(__dirname));

app.use(fileupload());

app.use('/', router);

app.listen(9999);

function isValidAPIKey(key) {
    return ImageHost.Keys.filter(x => x.Key == key).length > 0;
}

function getUserInfo(key) {
    return ImageHost.Keys.filter(y => y.Key == key)[0];
}

function hasValidExtension(name) {
    if (!WhitelistedExtensions.includes(name.split('.')[1])) {
        return false;
    }
    
    if (name.split('.').length > 0) {
        return true;
    }

    if (name.split('.').length === 0) {
        return false;
    }

    return true;
}

function isValidDomain(domain) {
    for(var i = 0; i < ImageHost.Domains.length; i++)
    {
        var Dom = ImageHost.Domains[i];
        if (Dom.Domain == domain) {
            return true;
        } else if (domain.includes(Dom.Domain)) {
            return true;
        }
    }

    return false;
}

function getDomainInfo(domain) {
    for(var i = 0; i < ImageHost.Domains.length; i++)
    {
        var Dom = ImageHost.Domains[i];
        if (Dom.Domain == domain) {
            return Dom;
        } else if (domain.includes(Dom.Domain)) {
            return Dom;
        }
    }

    return null;
}

function GenerateString(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
